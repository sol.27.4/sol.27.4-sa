---
output:
  word_document
---


# 3 Sole (Solea solea) in Subarea 4 (North Sea)

## 3.1 Stock ID and sub-stock structure

No new information was available on stock identity and sub-stock structure since the previous benchmark (WKNSEA 2015). An overview is available in the WKNSEA 2015 report (ICES, 2015).

## 3.2 Issue list

## 3.3 Scorecard on data quality

No scorecard was developed for this report.

## 3.4 Multispecies and mixed fisheries issues

## 3.5 Ecosystem drivers

## 3.6 Stock Assessment

### Catch – quality, misreporting, discards

Discards estimates for ages 5 and older show values in recent years that are difficult to exdplain given the dynamics of the fishery. Discards of fish under the minimum landing size are common in the fishery, which is currently not affected from the landing obligation regulations through a *de minimis* exemption (XX). In contrast, discards of valuable large sole should not be expected, specially given that catches for most fleets have not reached the annual TACs. A detailed investigation of the origin of these estimates is to be conducted and presented to the next WGNSSK meeting.

### Surveys

Four trawl surveys are currently carried out that should provide information on population trends for North Sea sole:

- BTS-ISIS (Beam Trawl Survey, from 1985 until now, the Netherlands).
- SNS (Sole Net Survey, from 1970 until now, the Netherlands).
- BTS-Belgica (Beam Trawl Survey, from 2004 until now, Belgium).
- BTS-Solea (Beam Trawl Survey, from 1976 until 2012, Germany).

Furthermore, the BTS-Tridens Beam Trawl Survey, carried out by the Netherlands from XX until now, covers areas North of BTS-ISIS where sole is expected to be absent or only found ocasionally. But recent analyses of the BTS-Tridens data (XX) has shown an increasing presence of sole in more Northern latitudes.

During the previous benchmark, the first two of those indices were selected as inputs to the stock assessment due to the identified problems with the data from the Belgium and German surveys. New data from these two surveys has been uploaded to ICES Datras since the last benchmark.

An standardized index of abundance for North Sea sole, based on the various BTS datasets (BTS-ISIS, BTS-Tridens, BTS-Belgica and BTS-Solea), has been developed and presented to WKFlatNSCS 2020. The index has been combined with the SNS dataset for use in the stock assessment, as in the previous benchmark (ICES, 2015).




### Weights, maturities, growth

### Natural mortality

Natural mortality in the period 1957–2018 has been assumed constant over all ages at
0.1, except for 1963 where a value of 0.9 was used to take into account the effect of the severe winter (1962-1963).

### Assessment model

Models run

Description of AAP

The model has been slightly refined from that used in the previous benchmark. The age dimension of the fishing mortality tensor spline, and the spline used to model catch selectivity at age, used to be defined with the same number of bases (knots). These two variables have now been separated, to allow the model greater flexibility. This change was intended to ameliorate the patterns in residuals on the fit to landings-at-age data for ages 2 and 3 observed in WGNSSK 2019 (ICES, 2019).

An R package is also now available for the model, which can be installed on multiple platforms. This facilitates, for example, incorporating model runs using AAP to the ICES TAF platform.

Results of AAP run(s)

- Retrospective

Configuration of AAP final run

SAM and a4a runs

Comparison of runs

- Results
- Restrospective



## 3.7 Short term projections

## 3.8 Appropriate Reference Points (MSY)

Setup of eqsim

- Type 2
- SRRs
- Years selex, wts

## 3.9 Future Research and data requirements

## 3.10 External Reviewers Comments
