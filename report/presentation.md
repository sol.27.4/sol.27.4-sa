---
title: "Stock assessment of North Sea sole (sol.27.4) using the Aarts & Poos (AAP) model."
subtitle: "ICES WKFlatNSCS benchmark. Copenhagen, 17-21 February 2020"
author: Iago MOSQUEIRA
institute: Wageningen Marine Research (WMR), IJmuiden, The Netherlands.
fontsize: 10pt
output:
  wmrkdown::wur:
    slide_level: 1
header-includes:
  - \newcommand{\pptfigure}[2][0.65]{\centering\includegraphics[width=#1\textwidth]{figures/#2}}
custom: images/4m21f.jpg
tags: [sol.27.4 SA]
---

# WGNSSK 2019 ISSUE LIST

## Tuning series

- Evaluate Belgium and German BTS index.
- Explore combining surveys.

## Assessment

- Assessment residual patterns age 2-3 in landings.

## Forecast

- Review forecast procedure (rct3).

## Biological Reference Points

- Determine MSY (proxy) reference points. Depending on the assessment method and available data.

# BTS INDICES OF ABUNDANCE

\pptfigure[0.65]{btsurveys-1.png}

# BTS GAM INTERNAL CONSISTENCY

\pptfigure[0.65]{survey/plot_corrtradindex-1.png}

# SNS INDEX OF ABUNDANCE

\pptfigure[0.65]{snssurvey-1.png}

# SNS INTERNAL CONSISTENCY

\pptfigure[0.65]{sns_corr-1.png}

# LANDINGS AND DISCARDS

\pptfigure[0.65]{catchseries-1.png}

# LANDINGS AND DISCARDS

\pptfigure[0.65]{bubblecatch-1.png}

# CHANGES TO AAP

```
control <- AAP.control(pGrp=TRUE, qplat.surveys=7,
  qplat.Fmatrix=9, Sage.knots=6, Fage.knots=8,
  Ftime.knots=28, Wtime.knots=5, mcmc=FALSE)
```

- `F ~ te(age, year)`
- `S ~ s(age)`

# MODEL SETUP

| Setting / data | Value / source                    |
|----------------|-----------------------------------|
| Catch-at-age   | Landings (since 1957, ages 1-10)  |
|                | Discards (since 1957, ages 1-10)  |
| Tuning indices | BTS-GAM (since 1985, ages 1-9)    |
|                | SNS (since 1970, ages 1-6)        |
| Plus group     | 10                                |
| First tuning year                            | 1970|
| Catchability catches constant for age >=     | 9   |
| Catchability surveys constant for ages >=    | 7   |
| Tensor spline for catchability-at-age survey | 6   |
| Tensor spline for F-at-age, ages             | 8   |
| Tensor spline for F-at-age, years            | 28  |

# AAP BASE CASE OUTPUT

\pptfigure[0.55]{aapbase_ssbfrec-1.png}

# FISHING MORTALITY

\pptfigure[0.55]{aapbase_fatagets-1.png}

# RESIDUALS

\pptfigure[0.65]{aapbase_bubbleresiduals-1.png}

# RESIDUALS

\centering\includegraphics[width=0.40\textwidth]{images/resid2019.png}
\pptfigure[0.55]{aapbase_bubbleresiduals-1.png}

# CATCH SELECTIVITIES

\pptfigure[0.45]{fleetselex-1.png}

# SURVEY SELECTIVITIES

\pptfigure[0.45]{survselex-1.pdf}

# PROPORTION OF SSB BY AGE

\pptfigure[0.55]{propssb-1.png}

# RETROSPECTIVE PATTERN

\pptfigure[0.55]{aapbase_retro2-1.png}

# COMPARE RUNS

\pptfigure[0.55]{runsrecent-1.png}
