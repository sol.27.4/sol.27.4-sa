data:
	R -e "source('data.R')"

input:
	R -e "source('input.R')"

model:
	R -e "source('model.R')"

output:
	R -e "source('output.R')"

report: report/report.docx

report/report.docx: report/report.Rmd
	R -e "source('report.R')"
